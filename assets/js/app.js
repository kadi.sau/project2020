//Globaalsed muutujad
let checklist;

// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    setUpHeaderBar();

    //Lisab tänase päeva
    document.querySelector("#logDay").value = (new Date()).toISOString().substr(0, 10);
    document.querySelector("#logDay").addEventListener('change', handleDateChange);

    doLoadChecklist();
});

//Vaja küsida hetkekuupäeva
function handleDateChange() {
    let selectedDate = document.querySelector("#logDay").value;
    let filteredList = checklist.filter(item => item.log_day == selectedDate);
    displayChecklistItems(filteredList);
    handleCrewmember(filteredList);
}

//Vaja küsida meeskonnaliikme kasutajat
function handleCrewmember(filteredList) {
    let selectedCrewmemberElement = document.querySelector("#inputGroupSelect02");
    let crewMember = filteredList.find(item => item.crew_member != null);
    let filteredCrewmember = crewMember ? crewMember.crew_member.toUpperCase() : '';
    selectedCrewmemberElement.value = filteredCrewmember;
}

function hasClass(ele, cls) {
    return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(ele, cls) {
    if (!hasClass(ele, cls)) ele.className += " " + cls;
}

function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
    let usernameElement = document.querySelector('#loginUsername');
    let passwordElement = document.querySelector('#loginPassword');
    let credentials = {
        username: usernameElement.value,
        password: passwordElement.value
    };
    let loginResponse = await postCredentials(credentials);
    if (loginResponse.id > 0) {
        localStorage.setItem('LOGIN_USERNAME', loginResponse.username);
        localStorage.setItem('LOGIN_TOKEN', loginResponse.token);
        setUpHeaderBar();
        closePopup();
        doLoadChecklist();
    } else {
        // Ilmnes mingi jama, lahendame selle siin...
        doLogout();
        displayLoginPopup();
    }
}

function doLogout() {
    localStorage.removeItem('LOGIN_USERNAME');
    localStorage.removeItem('LOGIN_TOKEN');
    displayLoginPopup();
}

function loadChecklist(checklist, element, element_id, checklistRowConfig = { quantityInput: true, morningInput: true, nightInput: true }) {
    let element_name = checklist.find(ci => ci.element_name === element);
    let commentInput = document.querySelector(`${element_id} [name=comment]`);
    let quantityInput = document.querySelector(`${element_id} input[name=quantity]`);
    let morningInput = document.querySelector(`${element_id} input[name=morning]`);
    let nightInput = document.querySelector(`${element_id} input[name=night]`);

    if (element_name) {
        commentInput.value = element_name.comment;

        if (checklistRowConfig.quantityInput) {
            if (element_name.quantity > 0){
                quantityInput.value = element_name.quantity;
            } else {
                quantityInput.value = '';
            }       
        }

        if (checklistRowConfig.morningInput) {
            morningInput.checked = element_name.day_check;
        }

        if (checklistRowConfig.nightInput) {
            nightInput.checked = element_name.night_check;
        }
    } else {
        commentInput.value = "";

        if (checklistRowConfig.quantityInput) {
            quantityInput.value = "";
        }

        if (checklistRowConfig.morningInput) {
            morningInput.checked = "";
        }

        if (checklistRowConfig.nightInput) {
            nightInput.checked = "";
        }
    }
}


async function doLoadChecklist() {
    console.log('Loading checklist...');
    checklist = await fetchChecklist();

    if (checklist) {
        displayChecklistItems(checklist);
        handleDateChange();
    } else {
        displayLoginPopup();
    }
}

function displayChecklistItems(filteredItems) {
    //Kuvamine kopterid valmisolekus
    loadChecklist(filteredItems, "ES-PWA", '#esPwa', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "ES-PWC", '#esPwb', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "ES-PWC", '#esPwc', { quantityInput: false, morningInput: true, nightInput: true });
    //Kuvamine: Varustus sar aedikus elemendid
    loadChecklist(filteredItems, "Päästekorv", '#rescueBasket');
    loadChecklist(filteredItems, "Medevac IIA raam", '#medevacIIAResqueLitter');
    loadChecklist(filteredItems, "Silmused GQ ja QS", '#slingGqandQs');
    loadChecklist(filteredItems, "Maandus", '#grounding');
    loadChecklist(filteredItems, "Kannatanu prillid", '#goggles');
    loadChecklist(filteredItems, "Kannatanu kõrvaklapid", '#protectiveHaedphones');
    loadChecklist(filteredItems, "Medvac IIA raamikate", '#medevacIIALitterCover');
    loadChecklist(filteredItems, "Õhuballoon", '#airTank', { quantityInput: true, morningInput: false, nightInput: false });
    //Kuvamine: med. rack elemendid
    loadChecklist(filteredItems, "Perfuusori kinnitused", '#perfusorAttachment');
    loadChecklist(filteredItems, "Hamiltoni kinnitus", '#hamiltonAttachment');
    loadChecklist(filteredItems, "Defibrillaatori kinnitus", '#defibrilatorAttachment');
    loadChecklist(filteredItems, "Infusiooni konksud", '#infusionHooks');
    loadChecklist(filteredItems, "Hapnik", '#oxygen', { quantityInput: true, morningInput: false, nightInput: false });
    loadChecklist(filteredItems, "Ballooni red. võti", '#tankWrench');
    loadChecklist(filteredItems, "Kõrvaklapid patsiendile", '#protectiveHeadphones');
    //Kuvamine: isiklik varustus
    loadChecklist(filteredItems, "Varustus vees töötamiseks ja vintsimiseks", '#landHoistEquipment', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Varustus kuivas keskkonnas töötamiseks ja vintsimiseks", '#waterHoistEquipment', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Ülik. mansetid ja lukud", '#zipsAndCuffs', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Päästerakmed(rihmad, CO2 padrun, vest)", '#harness', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Käelamp", '#torch', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Tri-Sirius Strobe", '#strobe', { quantityInput: false, morningInput: true, nightInput: false });
    //Kuvamine: kuivatusruum
    loadChecklist(filteredItems, "Kuiv varustus asetada oma kohale", '#dryingRoom', { quantityInput: false, morningInput: true, nightInput: false });
    //Kuvamine: meeskonna varustuse ruum 135
    loadChecklist(filteredItems, "Polycon käsijaamad: laetud vahetada", '#polycon', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "Puhastusvahendid (lapid, spreid)", '#cleaning', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Laadimisnurk", '#charging', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Tekid", '#blankets', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Bodybag", '#bodyBag', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Päästeparv ES-PWA", '#lifeRaftsEsPwA', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Päästeparv ES-PWB", '#lifeRaftsEsPwB', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Päästeparv ES-PWC", '#lifeRaftsEsPwC', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Juhtliin (roheline)", '#guidingLine', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, 'GQ lastekott', "#gqChildRescueValise", { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "RescuePack", '#rescuePack', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Medevac raam", '#medevacLitter', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Varustuse tõstekotid", '#equipmentBag', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Medevac IIA (reserv)", '#medevacIIAReserve', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Päästekorv (fold. reserv)", '#chemlight', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Keemil. valgus", '#rescueFlare', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Käsitõrvik", '#torch', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Suitsu padrun", '#distressSmoke', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Raketid", '#signalRockets', { quantityInput: false, morningInput: true, nightInput: false });
    loadChecklist(filteredItems, "Põrandakinnitused", '#helicopterFloorAttachments', { quantityInput: false, morningInput: true, nightInput: false });
    //Kuvamine: kopteri koristus
    loadChecklist(filteredItems, "ES-PWA koristu", '#cleaningPwA', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "ES-PWB koristus", '#cleaningPwB', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "ES-PWC koristus", '#cleaningPwC', { quantityInput: false, morningInput: true, nightInput: true });
    //Kuvamine: varustus ES PWA
    loadChecklist(filteredItems, "Päästesilmused", '#rescuelings');
    loadChecklist(filteredItems, "Screamer", '#screamer');
    loadChecklist(filteredItems, "Juhtliinid", '#guidingLine');
    loadChecklist(filteredItems, "Helikopteri maandus", '#helicopterGrounding');
    loadChecklist(filteredItems, "Med.kott", '#medicalBag');
    loadChecklist(filteredItems, "Tõstekarbiin", '#safetyLine');
    loadChecklist(filteredItems, "Turvarihm laes", '#carabiner');
    loadChecklist(filteredItems, "Raskuskott", '#weightBag');
    loadChecklist(filteredItems, "O-rõngas", '#oRing');
    loadChecklist(filteredItems, "Päästevestid", '#lifeJackets');
    loadChecklist(filteredItems, "Reisijate kõrvaklapid", '#paxHeadphones');
    loadChecklist(filteredItems, "Joogivesi", '#water');
    loadChecklist(filteredItems, "Lapse turvavöö", '#childSafetyBelt', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "Põrandakinnitused (4 nurga + 4 varustuse)", '#helicopterFloorAttachments', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "FLIR", '#flir', { quantityInput: false, morningInput: true, nightInput: true });
    //Kuvamine: varustus ES PWB
    loadChecklist(filteredItems, "Päästesilmused B", '#rescuelingsB');
    loadChecklist(filteredItems, "Screamer B", '#screamerB');
    loadChecklist(filteredItems, "Juhtliinid B", '#guidingLineB');
    loadChecklist(filteredItems, "Helikopteri maandus B", '#helicopterGroundingB');
    loadChecklist(filteredItems, "Med.kott B", '#medicalBagB');
    loadChecklist(filteredItems, "Tõstekarbiin B", '#safetyLineB');
    loadChecklist(filteredItems, "Turvarihm laes B", '#carabinerB');
    loadChecklist(filteredItems, "Raskuskott B", '#weightBagB');
    loadChecklist(filteredItems, "O-rõngas B", '#oRingB');
    loadChecklist(filteredItems, "Päästevestid B", '#lifeJacketsB');
    loadChecklist(filteredItems, "Reisijate kõrvaklapid B", '#paxHeadphonesB');
    loadChecklist(filteredItems, "Joogivesi B", '#waterB');
    loadChecklist(filteredItems, "Lapse turvavöö B", '#childSafetyBeltB', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "Põrandakinnitused (4 nurga + 4 varustuse) B", '#helicopterFloorAttachmentsB', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "FLIR B", '#flirB', { quantityInput: false, morningInput: true, nightInput: true });
    //Kuvamine: varustus ES PWC
    loadChecklist(filteredItems, "Päästesilmused C", '#rescuelingsC');
    loadChecklist(filteredItems, "Screamer C", '#screamerC');
    loadChecklist(filteredItems, "Juhtliinid C", '#guidingLineC');
    loadChecklist(filteredItems, "Helikopteri maandus C", '#helicopterGroundingC');
    loadChecklist(filteredItems, "Med.kott C", '#medicalBagC');
    loadChecklist(filteredItems, "Tõstekarbiin C", '#safetyLineC');
    loadChecklist(filteredItems, "Turvarihm laes C", '#carabinerC');
    loadChecklist(filteredItems, "Raskuskott C", '#weightBagC');
    loadChecklist(filteredItems, "O-rõngas C", '#oRingC');
    loadChecklist(filteredItems, "Päästevestid C", '#lifeJacketsC');
    loadChecklist(filteredItems, "Reisijate kõrvaklapid C", '#paxHeadphonesC');
    loadChecklist(filteredItems, "Joogivesi C", '#waterC');
    loadChecklist(filteredItems, "Lapse turvavöö C", '#childSafetyBeltC', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "Põrandakinnitused (4 nurga + 4 varustuse) C", '#helicopterFloorAttachmentsB', { quantityInput: false, morningInput: true, nightInput: true });
    loadChecklist(filteredItems, "FLIR C", '#flirC', { quantityInput: false, morningInput: true, nightInput: true });

}

// // Kuvame: Päästekorv - pikem versioon
// let rescueBasket = checklist.find(ci => ci.element_name === "Päästekorv");
// if (rescueBasket) {
//     let quantityInput = document.querySelector('#rescueBasket input[name=quantity]');
//     let morningInput = document.querySelector('#rescueBasket input[name=morning]');
//     let nightInput = document.querySelector('#rescueBasket input[name=night]');
//     let commentInput = document.querySelector('#rescueBasket [name=comment]');

//     quantityInput.value = rescueBasket.quantity;
//     morningInput.checked = rescueBasket.day_check;
//     nightInput.checked = rescueBasket.night_check;
//     commentInput.value = rescueBasket.comment;       
// }

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

async function displayLoginPopup() {
    openPopup(POPUP_CONF_BLANK_400_400, 'loginTemplate');
}

async function displayCompeteciesEditPopup() {
    await openPopup(POPUP_CONF_BLANK_600_900, 'competencies');
}

function setUpHeaderBar() {
    document.querySelector('#headerBar span').textContent = localStorage.getItem('LOGIN_USERNAME');
}

function editChecklistElement(element_id, checklistRowConfig = { quantityInput: true, morningInput: true, nightInput: true }) {
    if (element_id) {
        let checklistItem = {};

        let rowNameElement = document.querySelector(`${element_id} th`);
        checklistItem.element_name = rowNameElement.textContent;

        let commentInput = document.querySelector(`${element_id} [name=comment]`);
        checklistItem.comment = commentInput.value;

        if (checklistRowConfig.quantityInput) {
            let quantityInput = document.querySelector(`${element_id} input[name=quantity]`);
            checklistItem.quantity = quantityInput.value;
        }
        if (checklistRowConfig.morningInput) {
            let morningInput = document.querySelector(`${element_id} input[name=morning]`);
            checklistItem.day_check = morningInput.checked;
        }
        if (checklistRowConfig.nightInput) {
            let nightInput = document.querySelector(`${element_id} input[name=night]`);
            checklistItem.night_check = nightInput.checked;
        }
        return checklistItem;
    }
}

async function editChecklist() {
    let checklistItems = [];
    //Ehitame: kopterid valmisolekus elemente
    checklistItems.push(editChecklistElement('#esPwa', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#esPwb', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#esPwc', { quantityInput: false, morningInput: true, nightInput: true }));
    // //Ehitame: varustus sar aedikus elemente
    checklistItems.push(editChecklistElement('#rescueBasket'));
    checklistItems.push(editChecklistElement('#medevacIIAResqueLitter'));
    checklistItems.push(editChecklistElement('#slingGqandQs'));
    checklistItems.push(editChecklistElement('#grounding'));
    checklistItems.push(editChecklistElement('#goggles'));
    checklistItems.push(editChecklistElement('#protectiveHaedphones'));
    checklistItems.push(editChecklistElement('#airTank', { quantityInput: true, morningInput: false, nightInput: false }));
    //Ehitame med. rack elemente
    checklistItems.push(editChecklistElement('#perfusorAttachment'));
    checklistItems.push(editChecklistElement('#hamiltonAttachment'));
    checklistItems.push(editChecklistElement('#defibrilatorAttachment'));
    checklistItems.push(editChecklistElement('#infusionHooks'));
    checklistItems.push(editChecklistElement('#oxygen', { quantityInput: true, morningInput: false, nightInput: false }));
    checklistItems.push(editChecklistElement('#tankWrench'));
    checklistItems.push(editChecklistElement('#protectiveHeadphones'));
    //Ehitame: isiklik varustus elemente
    checklistItems.push(editChecklistElement('#landHoistEquipment', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#waterHoistEquipment', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#zipsAndCuffs', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#harness', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#torch', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#strobe', { quantityInput: false, morningInput: true, nightInput: false }));
    //Ehitame: kuivatusruum elemente
    checklistItems.push(editChecklistElement('#dryingRoom', { quantityInput: false, morningInput: true, nightInput: true }));
    //Ehitame: meeskonna varustuse ruum 135
    checklistItems.push(editChecklistElement('#polycon', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#cleaning', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#charging', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#blankets', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#bodyBag', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#lifeRaftsEsPwA', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#lifeRaftsEsPwB', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#lifeRaftsEsPwC', { quantityInput: false, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#guidingLine', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#gqChildRescueValise', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#rescuePack', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#medevacLitter', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#equipmentBag', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#medevacIIAReserve', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#chemlight', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#rescueFlare', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#torch', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#distressSmoke', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#signalRockets', { quantityInput: true, morningInput: true, nightInput: false }));
    checklistItems.push(editChecklistElement('#helicopterFloorAttachments', { quantityInput: true, morningInput: true, nightInput: false }));
    //Ehitame: kopteri koristus
    checklistItems.push(editChecklistElement('#cleaningPwA', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#cleaningPwB', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#cleaningPwC', { quantityInput: false, morningInput: true, nightInput: true }));
    //Ehitame: varustus ES-PWA
    checklistItems.push(editChecklistElement('#rescuelings'));
    checklistItems.push(editChecklistElement('#screamer'));
    checklistItems.push(editChecklistElement('#guidingLine'));
    checklistItems.push(editChecklistElement('#helicopterGrounding'));
    checklistItems.push(editChecklistElement('#medicalBag'));
    checklistItems.push(editChecklistElement('#safetyLine'));
    checklistItems.push(editChecklistElement('#carabiner'));
    checklistItems.push(editChecklistElement('#weightBag'));
    checklistItems.push(editChecklistElement('#oRing'));
    checklistItems.push(editChecklistElement('#lifeJackets'));
    checklistItems.push(editChecklistElement('#paxHeadphones'));
    checklistItems.push(editChecklistElement('#water'));
    checklistItems.push(editChecklistElement('#childSafetyBelt', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#helicopterFloorAttachments', { quantityInput: false, morningInput: true, nightInput: true }));
    checklistItems.push(editChecklistElement('#flir', { quantityInput: false, morningInput: true, nightInput: true }));
     //Ehitame: varustus ES-PWB
     checklistItems.push(editChecklistElement('#rescuelingsB'));
     checklistItems.push(editChecklistElement('#screamerB'));
     checklistItems.push(editChecklistElement('#guidingLineB'));
     checklistItems.push(editChecklistElement('#helicopterGroundingB'));
     checklistItems.push(editChecklistElement('#medicalBagB'));
     checklistItems.push(editChecklistElement('#safetyLineB'));
     checklistItems.push(editChecklistElement('#carabinerB'));
     checklistItems.push(editChecklistElement('#weightBagB'));
     checklistItems.push(editChecklistElement('#oRingB'));
     checklistItems.push(editChecklistElement('#lifeJacketsB'));
     checklistItems.push(editChecklistElement('#paxHeadphonesB'));
     checklistItems.push(editChecklistElement('#waterB'));
     checklistItems.push(editChecklistElement('#childSafetyBeltB', { quantityInput: false, morningInput: true, nightInput: true }));
     checklistItems.push(editChecklistElement('#helicopterFloorAttachmentsB', { quantityInput: false, morningInput: true, nightInput: true }));
     checklistItems.push(editChecklistElement('#flirB', { quantityInput: false, morningInput: true, nightInput: true }));
     //Ehitame: varustus ES-PWC
     checklistItems.push(editChecklistElement('#rescuelingsC'));
     checklistItems.push(editChecklistElement('#screamerC'));
     checklistItems.push(editChecklistElement('#guidingLineC'));
     checklistItems.push(editChecklistElement('#helicopterGroundingC'));
     checklistItems.push(editChecklistElement('#medicalBagC'));
     checklistItems.push(editChecklistElement('#safetyLineC'));
     checklistItems.push(editChecklistElement('#carabinerC'));
     checklistItems.push(editChecklistElement('#weightBagC'));
     checklistItems.push(editChecklistElement('#oRingC'));
     checklistItems.push(editChecklistElement('#lifeJacketsC'));
     checklistItems.push(editChecklistElement('#paxHeadphonesC'));
     checklistItems.push(editChecklistElement('#waterC'));
     checklistItems.push(editChecklistElement('#childSafetyBeltC', { quantityInput: false, morningInput: true, nightInput: true }));
     checklistItems.push(editChecklistElement('#helicopterFloorAttachmentsC', { quantityInput: false, morningInput: true, nightInput: true }));
     checklistItems.push(editChecklistElement('#flirC', { quantityInput: false, morningInput: true, nightInput: true }));

    // // Ehitame: Päästekorv - pikem versioon
    // quantityInput = document.querySelector('#rescueBasket input[name=quantity]');
    // morningInput = document.querySelector('#rescueBasket input[name=morning]');
    // nightInput = document.querySelector('#rescueBasket input[name=night]');
    // commentInput = document.querySelector('#rescueBasket [name=comment]');

    // checklistItem = {
    //     quantity: quantityInput.value,
    //     day_check: morningInput.checked,
    //     night_check: nightInput.checked,
    //     comment: commentInput.value
    // };
    // checklistItems.push(checklistItem);

    let logSet = {
        log_day: document.querySelector("#logDay").value,
        crew_member: document.querySelector("#inputGroupSelect02").value,
        checklistElementsList: checklistItems
    };
    //console log hetkel sees
    console.log(logSet);
    await postChecklist(logSet);
    await doLoadChecklist();
    openSuccessAlert();
}

//Avame ja sulgeme tabeli päise klikiga
function toggleTableBodyVisibility(tableId) {
    let theTable = document.querySelector(`${tableId}`);

    if (hasClass(theTable, 'hidden')) {
        removeClass(theTable, 'hidden');
    } else {
        addClass(theTable, 'hidden');
    }

    //Teine versioon tabeli päise avamiseks ja sulgemiseks
    // if (theTable.style.display === 'none') {
    //     theTable.style.display = '';
    // } else {
    //     theTable.style.display = 'none';
    // }
}


//Avame ja sulgeme Success alerti peale salvestamist
function closeSuccessAlert() {
    let alert = document.querySelector('#saveSuccessDiv');
    alert.style.display = 'none';
}

function openSuccessAlert() {
    let alert = document.querySelector('#saveSuccessDiv');
    alert.style.display = 'block';
}
