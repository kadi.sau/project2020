async function postCredentials(credentials) {
    try {
        const response = await fetch(`${API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        });
        return await response.json();
    } catch (e) {
        console.log('LOGIN FAILED', e)
    }
}

async function fetchChecklist() {
    try {
        const response = await fetch(`${API_URL}/checklists/all`, {
            method: 'GET',
            headers: {
                'Authorization': composeBearerToken()
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function postChecklist(checklist){
    try {
        const response = await fetch(`${API_URL}/checklists/edit`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(checklist)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING CHECKLIST', e);
    }
}

function composeBearerToken() {
    return `Bearer ${localStorage.getItem('LOGIN_TOKEN')}`;
}

async function processProtectedResponse(response) {
    if (response.status >= 400 && response.status <= 403) {
        throw new Error('Unauthorized');
    } else {
        return await response.json();
    }
}
